Given(/^I am logged into ToDo list application with user "([^"]*)" and password "([^"]*)"$/) do |user, password|
  Tasks.new.log_user(user, password)
end

Given (/^I am on "([^"]*)" page$/) do |page|
  Tasks.new.visit_page(page)
end

When(/^I insert the value "([^"]*)" in the "([^"]*)" field$/) do |value, field_name|
  Tasks.new.fill_title_field(value, field_name)
end

When(/^I submit the "([^"]*)" form$/) do |form|
  Tasks.new.submit_form(form)
end

When(/^I click to open the substask modal$/) do
  Tasks.new.open_subtask_modal()
end

When (/^I click to remove a "([^"]*)"$/) do |item|
  Tasks.new.remove_item(item)
end

Then(/^the "([^"]*)" is created successfully with value "([^"]*)"$/) do |form, value|
  Tasks.new.validate_created_item(form, value)
end

Then(/^the "([^"]*)" is deleted$/) do |item|
  Tasks.new.validate_deleted_item(item)
end

Then (/^I close the modal$/) do
  Tasks.new.close_modal()
end

Then(/^the error message "([^"]*)" should be displayed$/) do |message|
  Tasks.new.error_message(message)
end