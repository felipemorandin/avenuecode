class Tasks
	include Capybara::DSL
	include RSpec::Matchers

	def log_user(user, password)
		visit Capybara.app_host + "users/sign_in"
		fill_in("user_email", :with => user)
		fill_in("user_password", :with => password)
		find("input.btn-primary").click
	end

	def visit_page(page)
		visit Capybara.app_host + page
	end

	def fill_title_field(value, field_name)
		if field_name == "tasks_title"
			fill_in("new_task", :with => value)
		elsif field_name == "sub_tasks_title"
			fill_in("new_sub_task", :with => value)
		elsif field_name == "due_date"
			fill_in("dueDate", :with => value)
		end
	end

	def submit_form(form)
		if form == "tasks"
			find("span.input-group-addon").click
		elsif form == "sub_tasks"
			find_by_id("add-subtask").click
		end
	end

	def validate_created_item(form, value)
		if form == "task"
			first("td.task_body a", :text => value)
		elsif form == "sub_task"
			first("div.modal-content td.task_body a", :text => value)
		end
	end

	def open_subtask_modal()
		first("tr.ng-scope button.btn-primary").click
	end

	def close_modal()
		find("div.modal-footer button.btn-primary").click
	end

	def remove_item(item)
		if item == "subtask"
			first("div.modal-body tr.ng-scope button.btn-danger").click
		elsif item == "task"
			first("div.panel-default tr.ng-scope button.btn-danger").click
		end
	end

	# This implementation was not tested since I'm not being able to 
	# reproduce this in the screen. The test is here, it's just a matter of
	# get the right HTML to check this
	def error_message(message)
		find("div.error_message", :text => message)
	end

	def validate_deleted_item(item)
		if item == "task"
			page.should have_no_selector("td.task_body a")
		elsif item == "subtask"
			page.should have_no_selector("div.modal-content td.task_body a")
		end
	end
end