#enconding: utf-8
Feature: Tasks Tests

    File that will test scenarios related with tasks
    Background:

        # Log the user in
    Given I am logged into ToDo list application with user "felipemorandin@gmail.com" and password "SenhaAvenueCode"
    And I am on "tasks" page

    Scenario: Tests for task and subtask's creation


        # Create the task
        When I insert the value "Task created in feature file" in the "tasks_title" field
            And I submit the "tasks" form
        Then the "task" is created successfully with value "Task created in feature file"

        # Create the subtask
        When I click to open the substask modal
            And I insert the value "Subtask created in feature file" in the "sub_tasks_title" field
            And I insert the value "07/20/2017" in the "due_date" field
            And I submit the "sub_tasks" form
        Then the "sub_task" is created successfully with value "Subtask created in feature file"
            And I close the modal

        # Remove subtask
        When I click to open the substask modal
            And I click to remove a "subtask"

        Then the "subtask" is deleted

        # Close the modal
            When I close the modal

        # Remove the task
        And I click to remove a "task"
        # Check if the task was removed
        Then the "task" is deleted

    Scenario: Task's title bigger than 250 characters


        # Create the task
        When I insert the value "abcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyz12345" in the "tasks_title" field
            And I submit the "tasks" form

        # This one is failing because it's possible to insert more than 250 characters in task's title
        Then the error message "The task can’t have more than 250 characters." should be displayed

    # This one is failing because it's possible to insert more than 250 characters in subtask's title
    Scenario: Subtask's title bigger than 250 characters

        When I insert the value "Task created in feature file" in the "tasks_title" field
            And I submit the "tasks" form
        When I click to open the substask modal

            # Create the task
            And I insert the value "abcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyz12345" in the "sub_tasks_title" field
            And I submit the "sub_tasks" form

        # This one is failing because it's possible to insert more than 250 characters in subtask's title
        Then the error message "The subtask can’t have more than 250 characters." should be displayed

    Scenario: Task title empty


        # Create the task
        When I insert the value "" in the "tasks_title" field
            And I submit the "tasks" form

        # This one is failing because it's possible to insert tasks with empty title
        Then the error message "The task name cannot be empty." should be displayed

    Scenario: Subtask title empty

        When I insert the value "Task created in feature file" in the "tasks_title" field
            And I submit the "tasks" form
        When I click to open the substask modal

        # Create the task
        When I insert the value "" in the "sub_tasks_title" field
            And I submit the "sub_tasks" form

        # This one is failing because it's possible to insert subtasks with empty title
        Then the error message "The subtask name cannot be empty." should be displayed

    Scenario: Subtask due date empty

        When I insert the value "Task created in feature file" in the "tasks_title" field
            And I submit the "tasks" form
        When I click to open the substask modal

        # Create the task
        When I insert the value " " in the "duedate" field
            And I insert the value "Subtask created in feature file" in the "sub_tasks_title" field
            And I submit the "sub_tasks" form

        # This one is failing because it's possible to insert 
        Then the error message "The task name cannot be empty." should be displayed