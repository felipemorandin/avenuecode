require 'rspec'
require 'rspec/expectations'
require 'capybara/cucumber'
require 'capybara/dsl'

chrome_driver = :chrome
Capybara.register_driver chrome_driver do |app|
	Capybara::Selenium::Driver.new(
		app,
		:browser => chrome_driver,
		desired_capabilities: {
			"chromeOptions" => {
				"args" => %w{ no-sandbox }
			}
		}
	)
end

Capybara.default_driver = chrome_driver
Capybara.app_host = "http://qa-test.avenuecode.com/"
Capybara.default_max_wait_time = 20